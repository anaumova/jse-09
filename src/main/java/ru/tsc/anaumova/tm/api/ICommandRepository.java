package ru.tsc.anaumova.tm.api;

import ru.tsc.anaumova.tm.model.Command;

public interface ICommandRepository {

    Command[] getTerminalCommands();

}