package ru.tsc.anaumova.tm.api;

public interface ICommandController {

    void showWelcome();

    void showAbout();

    void showVersion();

    void showCommands();

    void showArguments();

    void showHelp();

    void showSystemInfo();

    void showErrorArgument(String arg);

    void showErrorCommand(String arg);

}