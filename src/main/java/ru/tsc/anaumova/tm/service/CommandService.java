package ru.tsc.anaumova.tm.service;

import ru.tsc.anaumova.tm.api.ICommandRepository;
import ru.tsc.anaumova.tm.api.ICommandService;
import ru.tsc.anaumova.tm.model.Command;

public class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(final ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public Command[] getTerminalCommands() {
        return commandRepository.getTerminalCommands();
    }

}